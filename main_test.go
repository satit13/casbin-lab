package main

import (
	"fmt"
	"testing"

	"github.com/casbin/casbin"
	"github.com/stretchr/testify/assert"
)

func Test_merchant1_admin_allow_item_create(t *testing.T) {
	usr := "satit" // admin
	mer := "nopadol"
	menu := "item"
	act := "c" //true
	enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	result := enforcer.EnforceRbacDomain(usr, mer, menu, act)
	assert.Equal(t, result, true)

}

func Test_merchant1_use_deny_item_create(t *testing.T) {
	usr := "milk" // cashier
	mer := "nopadol"
	menu := "item"
	act := "c" // false
	enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	result := enforcer.EnforceRbacDomain(usr, mer, menu, act)
	assert.Equal(t, result, false)

}

func Test_merchant2_admin_allow_item_read(t *testing.T) {
	usr := "milk"
	mer := "makekafe"
	menu := "item"
	act := "r"
	enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	result := enforcer.EnforceRbacDomain(usr, mer, menu, act)
	assert.Equal(t, result, true)
}
func Test_merchant2_admin_deny_item_create(t *testing.T) {
	usr := "milk"
	mer := "makekafe"
	menu := "item"
	act := "c"
	enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	result := enforcer.EnforceRbacDomain(usr, mer, menu, act)
	assert.Equal(t, result, false)
}

func Test_deny_by_merchant1(t *testing.T) {
	usr := "milk"
	mer := "nopadol"
	menu := "item"
	act := "r"
	enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	result := enforcer.EnforceRbacDomain(usr, mer, menu, act)

	assert.Equal(t, result, false)
}

func Test_GetPermissionByUser(t *testing.T) {
	usr := "htom"
	// dom := "nopadol"
	// obj := "item"
	// act := "r"
	// enforcer := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	// enforcer := casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")
	ef := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	e := ef.enforcer
	allPolicys := e.GetFilteredGroupingPolicy(0, usr)

	fmt.Println(allPolicys)
	// assert.Equal(t, result, false)
}

func Test_GetRolesForUser(t *testing.T) {
	usr := "milk"
	mer := "nopadol"

	ef := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	e := ef.enforcer
	allPolicys := e.GetRolesForUserInDomain(usr, mer)
	fmt.Println(allPolicys)
	// assert.Equal(t, result, false)
}

func TestGetUserForRoleAdmin(t *testing.T) {
	role := "admin"
	dom := "nopadol"
	ef := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	e := ef.enforcer
	allPolicys := e.GetUsersForRoleInDomain(role, dom)
	fmt.Println(allPolicys)
	// assert.Equal(t, result, false)
}

func TestAddPolicyNewPolicy(t *testing.T) {
	NewRole := "cashier" // new role
	NewUsr := "big"
	dom := "nopadol"
	// menu := "item"
	// act := "r"
	expect := int(2)
	ef := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	e := ef.enforcer
	allPolicys := e.GetUsersForRoleInDomain(NewRole, dom) // add cashier -> nopadol domain
	fmt.Printf("\n before add new user to cashier-nopadol %v , count %v ", allPolicys, len(allPolicys))
	// allPolicys := e.GetUsersForRoleInDomain(role, dom)
	// e.AddNamedPolicy("p", NewRole, dom, menu, act)
	res := e.AddNamedGroupingPolicy("g", NewUsr, NewRole, dom)
	if res != true {
		t.Errorf("error add new group ")
	}
	allPolicys = e.GetUsersForRoleInDomain(NewRole, dom)
	got := len(allPolicys)

	fmt.Printf("\n After add big %v , count %v \n", allPolicys, got)
	assert.Equal(t, got, expect)

}

func TestGetPermissionList(t *testing.T) {
	usr := "satit"
	// dom := "nopadol"
	// menu := "item"
	// act := "r"
	// expect := int(2)
	ef := Enforcer{enforcer: casbin.NewEnforcer("model-rbac-domain.conf", "policy-rbac-domain.csv")}
	e := ef.enforcer
	allPremissions := e.GetAllSubjects() // add cashier -> nopadol domain
	fmt.Printf("\n get all permission by user %v , count %v ", usr, len(allPremissions))
	fmt.Println(allPremissions)

	// assert.Equal(t, got, expect)

}

// func TestRemovePolicy(t *testing.T) {

// }
